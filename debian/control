Source: python-cmislib
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Michael Fladischer <fladi@debian.org>
Build-Depends: debhelper-compat (= 9),
               dh-python,
               python-all,
               python-iso8601,
               python-setuptools,
               python-sphinx (>= 1.0.7+dfsg)
Standards-Version: 3.9.6
Homepage: https://chemistry.apache.org/python/cmislib.html
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-cmislib
Vcs-Git: https://salsa.debian.org/python-team/packages/python-cmislib.git

Package: python-cmislib
Architecture: all
Depends: python-iso8601,
         ${misc:Depends},
         ${python:Depends}
Suggests: python-cmislib-doc
Description: CMIS client library for Python
 Apache Chemistry cmislib is a CMIS client library for Python.
 .
 The goal of this library is to provide an interoperable API to CMIS
 repositories such as Alfresco, Nuxeo, KnowledgeTree, MS SharePoint, EMC
 Documentum, and any other content repository that is CMIS-compliant.

Package: python-cmislib-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: CMIS client library for Python (documentation)
 Apache Chemistry cmislib is a CMIS client library for Python.
 .
 The goal of this library is to provide an interoperable API to CMIS
 repositories such as Alfresco, Nuxeo, KnowledgeTree, MS SharePoint, EMC
 Documentum, and any other content repository that is CMIS-compliant.
 .
 This package contains the documentation.
